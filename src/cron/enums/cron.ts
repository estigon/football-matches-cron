export enum GameStatus {
    SCHEDULED = 1,
    FINISHED = 2
}

export enum TaskStatusCron {
    STARTED = 1,
    FINISHED = 2
}

export enum CronType {
    GET_SCHEDULED = 1,
    GET_FINISHED = 2
}
