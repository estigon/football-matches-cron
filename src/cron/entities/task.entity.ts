import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({name: 'task_status_id'})
  taskStatus: number;

  @Column()
  error?: string;

  @Column({name: 'type_id'})
  type: number;
}