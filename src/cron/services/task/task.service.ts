import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from 'src/cron/entities';

@Injectable()
export class TaskService {
    constructor(
        @InjectRepository(Task) private taskRepository : Repository<Task>
    ){}

    async create(body: any): Promise<Task> {
        const newTask = this.taskRepository.create(body as any);
        const task = await this.taskRepository.save(newTask);
        return task as unknown as Task;
    }

    async update(id: number, body: any) {
        const booking = await this.taskRepository.findOne({ where: { id }});
        if(booking){
            this.taskRepository.merge(booking, body);
            return this.taskRepository.save(booking);
        }
        throw new NotFoundException(`No se encuentra la tarea con id ${id}.`)
    }
}
