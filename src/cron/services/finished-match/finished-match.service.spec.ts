import { Test, TestingModule } from '@nestjs/testing';
import { FinishedMatchService } from './finished-match.service';

describe('FinishedMatchService', () => {
  let service: FinishedMatchService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FinishedMatchService],
    }).compile();

    service = module.get<FinishedMatchService>(FinishedMatchService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
