import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Match, MatchStatus } from 'src/cron/entities';
import { CronType, GameStatus, TaskStatusCron } from 'src/cron/enums/cron';
import { getRequest, baseURL } from 'src/utils/axiosClient';
import { Repository } from 'typeorm';
import { TaskService } from '../task/task.service';

@Injectable()
export class FinishedMatchService {
    private readonly logger = new Logger(FinishedMatchService.name);
    private readonly season = 2022;
    private readonly FINISHED = 'FINISHED';
    private readonly competitions = 'BSA';
    private readonly FINISHED_URL = `/v4/competitions/${this.competitions}/matches?season=${this.season}&status=${this.FINISHED}`;

    constructor(
        private taskService: TaskService,
        @InjectRepository(Match) private matchRepository : Repository<Match>,
        @InjectRepository(MatchStatus) private matchStatusRepository : Repository<MatchStatus>,
    ){}

    @Cron("0 */6 * * * *")
    async handleCronFinishedMatch() {
        this.logger.debug('Called handleCronFinishedMatch EVERY_6_MINUTES');
        let taskId = 0;

        try {
            // create new cron task with status started
            const task = await this.taskService.create({
                taskStatus: TaskStatusCron.STARTED,
                type: CronType.GET_FINISHED,
            });
            taskId = task.id;            

            // make request
            const response = await getRequest(`${baseURL}${this.FINISHED_URL}`);
            this.logger.log(`GET API DATA - TASK ${taskId}`);

            // get match status finished from database
            const matchStatus = await this.matchStatusRepository.findOne({ where: { id: GameStatus.FINISHED }});
            this.logger.log(`GET MATCH STATUS - TASK ${taskId}`);

            // I go through all the matches
            for (const matchRaw of response.data.matches) {
                
                const { id, score } = matchRaw;

                // look if the match is already saved in database with status schedule
                const matchAlreadySaved = await this.matchRepository.findOne({
                    where: { id , matchStatus: {id : GameStatus.SCHEDULED} },
                    relations: {
                        awayTeam: true,
                        homeTeam: true,
                        matchStatus: true
                    }
                });

                this.logger.log(`IS MATCH ${id} ALREADY SAVED ${matchAlreadySaved ? 'YES' : 'NO'} - TASK ${taskId}`);

                // if exist update data
                if(matchAlreadySaved) {

                    const { teamName: homeTeamName } = matchAlreadySaved.homeTeam;
                    const { teamName: awayTeamName } = matchAlreadySaved.awayTeam;
                    const matchScore = `${score.fullTime.home} - ${score.fullTime.away}`;

                    const updatedMatchData = {
                        ...matchAlreadySaved,
                        matchStatus: matchStatus,
                        score: matchScore,
                        winner : this.matchWinner(score.winner, awayTeamName, homeTeamName)
                    }
                    
                    // save updated data
                    this.matchRepository.merge(matchAlreadySaved, updatedMatchData);
                    await this.matchRepository.save(matchAlreadySaved);
                    this.logger.log(`MATCH ${id} UPDATED - TASK ${taskId}`);
                }

            }

            // put cron task status to ended
            task.taskStatus = TaskStatusCron.FINISHED;
            await this.taskService.update(taskId, task);
            this.logger.log(`TASK FINISHED - TASK ${taskId}`);

        } catch (error) {
            const { message } = error;
            if(taskId !== 0) {
                const task = {
                    id: taskId,
                    taskStatus: TaskStatusCron.FINISHED,
                    type: CronType.GET_FINISHED,
                    error: message
                };
                // put cron task status to ended and add error
                await this.taskService.update(taskId, task);
            }
            this.logger.error(message);
        }
    }

    matchWinner(winner: string, away: string, home: string) : string{
        if(winner === 'DRAW'){
            return 'DRAW';
        }
        if(winner === 'AWAY_TEAM'){
            return away;
        }
        if(winner === 'HOME_TEAM'){
            return home;
        }
    }
}
