export const response = {
    data: {
        matches: [
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390024,
                "utcDate": "2022-04-09T19:30:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 1765,
                    "name": "Fluminense FC",
                    "shortName": "Fluminense",
                    "tla": "FLU",
                    "crest": "https://crests.football-data.org/1765.svg"
                },
                "awayTeam": {
                    "id": 6685,
                    "name": "Santos FC",
                    "shortName": "Santos",
                    "tla": "SAN",
                    "crest": "https://crests.football-data.org/6685.svg"
                },
                "score": {
                    "winner": "DRAW",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 0,
                        "away": 0
                    },
                    "halfTime": {
                        "home": 0,
                        "away": 0
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390025,
                "utcDate": "2022-04-09T22:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 3988,
                    "name": "AC Goianiense",
                    "shortName": "AC Goianiense",
                    "tla": "ACG",
                    "crest": "https://crests.football-data.org/3988.svg"
                },
                "awayTeam": {
                    "id": 1783,
                    "name": "CR Flamengo",
                    "shortName": "Flamengo",
                    "tla": "FLA",
                    "crest": "https://crests.football-data.org/1783.png"
                },
                "score": {
                    "winner": "DRAW",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 1,
                        "away": 1
                    },
                    "halfTime": {
                        "home": 0,
                        "away": 0
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390026,
                "utcDate": "2022-04-10T00:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 1769,
                    "name": "SE Palmeiras",
                    "shortName": "Palmeiras",
                    "tla": "PAL",
                    "crest": "https://crests.football-data.org/1769.png"
                },
                "awayTeam": {
                    "id": 1837,
                    "name": "Ceará SC",
                    "shortName": "Ceará",
                    "tla": "CSC",
                    "crest": "https://crests.football-data.org/1837.svg"
                },
                "score": {
                    "winner": "AWAY_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 2,
                        "away": 3
                    },
                    "halfTime": {
                        "home": 1,
                        "away": 2
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390027,
                "utcDate": "2022-04-10T14:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 4241,
                    "name": "Coritiba FBC",
                    "shortName": "Coritiba",
                    "tla": "COR",
                    "crest": "https://crests.football-data.org/4241.svg"
                },
                "awayTeam": {
                    "id": 4250,
                    "name": "Goiás EC",
                    "shortName": "Goiás",
                    "tla": "GOI",
                    "crest": "https://crests.football-data.org/4250.svg"
                },
                "score": {
                    "winner": "HOME_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 3,
                        "away": 0
                    },
                    "halfTime": {
                        "home": 1,
                        "away": 0
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390028,
                "utcDate": "2022-04-10T19:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 1766,
                    "name": "CA Mineiro",
                    "shortName": "Mineiro",
                    "tla": "CAM",
                    "crest": "https://crests.football-data.org/1766.png"
                },
                "awayTeam": {
                    "id": 6684,
                    "name": "SC Internacional",
                    "shortName": "Internacional",
                    "tla": "SCI",
                    "crest": "https://crests.football-data.org/6684.svg"
                },
                "score": {
                    "winner": "HOME_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 2,
                        "away": 0
                    },
                    "halfTime": {
                        "home": 1,
                        "away": 0
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390029,
                "utcDate": "2022-04-10T19:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 1770,
                    "name": "Botafogo FR",
                    "shortName": "Botafogo",
                    "tla": "BOT",
                    "crest": "https://crests.football-data.org/1770.svg"
                },
                "awayTeam": {
                    "id": 1779,
                    "name": "SC Corinthians Paulista",
                    "shortName": "Corinthians",
                    "tla": "COR",
                    "crest": "https://crests.football-data.org/1779.svg"
                },
                "score": {
                    "winner": "AWAY_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 1,
                        "away": 3
                    },
                    "halfTime": {
                        "home": 0,
                        "away": 3
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390030,
                "utcDate": "2022-04-10T21:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 3984,
                    "name": "Fortaleza EC",
                    "shortName": "Fortaleza",
                    "tla": "FEC",
                    "crest": "https://crests.football-data.org/3984.png"
                },
                "awayTeam": {
                    "id": 4289,
                    "name": "Cuiabá EC",
                    "shortName": "Cuiabá EC",
                    "tla": "CUI",
                    "crest": "https://crests.football-data.org/4289_large.png"
                },
                "score": {
                    "winner": "AWAY_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 0,
                        "away": 1
                    },
                    "halfTime": {
                        "home": 0,
                        "away": 1
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            },
            {
                "area": {
                    "id": 2032,
                    "name": "Brazil",
                    "code": "BRA",
                    "flag": "https://crests.football-data.org/764.svg"
                },
                "competition": {
                    "id": 2013,
                    "name": "Campeonato Brasileiro Série A",
                    "code": "BSA",
                    "type": "LEAGUE",
                    "emblem": "https://crests.football-data.org/764.svg"
                },
                "season": {
                    "id": 1377,
                    "startDate": "2022-04-10",
                    "endDate": "2022-11-13",
                    "currentMatchday": 8,
                    "winner": null
                },
                "id": 390031,
                "utcDate": "2022-04-10T22:00:00Z",
                "status": "FINISHED",
                "matchday": 1,
                "stage": "REGULAR_SEASON",
                "group": null,
                "lastUpdated": "2022-05-28T16:20:22Z",
                "homeTeam": {
                    "id": 1776,
                    "name": "São Paulo FC",
                    "shortName": "São Paulo",
                    "tla": "PAU",
                    "crest": "https://crests.football-data.org/1776.svg"
                },
                "awayTeam": {
                    "id": 1768,
                    "name": "CA Paranaense",
                    "shortName": "Paranaense",
                    "tla": "CAP",
                    "crest": "https://crests.football-data.org/1768.png"
                },
                "score": {
                    "winner": "HOME_TEAM",
                    "duration": "REGULAR",
                    "fullTime": {
                        "home": 4,
                        "away": 0
                    },
                    "halfTime": {
                        "home": 1,
                        "away": 0
                    }
                },
                "odds": {
                    "msg": "Activate Odds-Package in User-Panel to retrieve odds."
                },
                "referees": []
            }
        ]
    }
}