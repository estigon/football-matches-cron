import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import { Match, MatchStatus, Team } from 'src/cron/entities';
import { CronType, GameStatus, TaskStatusCron } from 'src/cron/enums/cron';
import { getRequest, baseURL } from 'src/utils/axiosClient';
import { Repository } from 'typeorm';
import { TaskService } from '../task/task.service';

@Injectable()
export class ScheduledMatchService {
    private readonly logger = new Logger(ScheduledMatchService.name);
    private readonly season = 2022;
    private readonly SCHEDULED = 'SCHEDULED';
    private readonly competitions = 'BSA';
    private readonly SCHEDULE_URL = `/v4/competitions/${this.competitions}/matches?season=${this.season}&status=${this.SCHEDULED}`;

    constructor(
        private taskService: TaskService,
        @InjectRepository(Match) private matchRepository : Repository<Match>,
        @InjectRepository(Team) private teamRepository : Repository<Team>,
        @InjectRepository(MatchStatus) private matchStatusRepository : Repository<MatchStatus>,
    ){}

    @Cron(CronExpression.EVERY_5_MINUTES)
    async handleCronScheduledMatch() {
        this.logger.debug('Called handleCronScheduledMatch EVERY_5_MINUTES');
        let taskId = 0;

        try {
            // create new cron task with status started
            const task = await this.taskService.create({
                taskStatus: TaskStatusCron.STARTED,
                type: CronType.GET_SCHEDULED,
            });

            taskId = task.id;            

            // make request
            const response = await getRequest(`${baseURL}${this.SCHEDULE_URL}`);
            this.logger.log(`GET API DATA - TASK ${taskId}`);

            // get match status scheduled from database
            const matchStatus = await this.matchStatusRepository.findOne({ where: { id: GameStatus.SCHEDULED }});
            this.logger.log(`GET MATCH STATUS - TASK ${taskId}`);

            // I go through all the matches
            for (const matchRaw of response.data.matches) {
                
                const { id, utcDate, homeTeam, awayTeam } = matchRaw;

                // look if the match is already saved in database
                const matchAlreadySaved = await this.matchRepository.findOne({where: { id }});
                this.logger.log(`IS MATCH ${id} ALREADY SAVED ${matchAlreadySaved ? 'YES' : 'NO'} - TASK ${taskId}`);

                // if it doesn't exist create a new one
                if(!matchAlreadySaved) {

                    // look if the home team is already saved in database
                    let homeTeamAlreadySaved = await this.teamRepository.findOne({where: { id: homeTeam.id }});
                    this.logger.log(`IS HOME TEAM ALREADY SAVED ${homeTeamAlreadySaved ? 'YES' : 'NO'} - TASK ${taskId}`);

                    // if it doesn't exist create a new one
                    if(!homeTeamAlreadySaved) {
                        homeTeamAlreadySaved = new Team();
                        homeTeamAlreadySaved.id = homeTeam.id;
                        homeTeamAlreadySaved.teamName = homeTeam.name;
                        await this.teamRepository.manager.save(homeTeamAlreadySaved)
                        this.logger.log(`HOME TEAM SAVED - TASK ${taskId}`);
                        
                        // await dataSource.manager.save(homeTeamAlreadySaved)
                    }

                    // look if the away team is already saved in database
                    let awayTeamAlreadySaved = await this.teamRepository.findOne({where: { id: awayTeam.id }});
                    this.logger.log(`IS AWAY TEAM ALREADY SAVED ${awayTeamAlreadySaved ? 'YES' : 'NO'} - TASK ${taskId}`);

                    // if it doesn't exist create a new one
                    if(!awayTeamAlreadySaved) {
                        awayTeamAlreadySaved = new Team();
                        awayTeamAlreadySaved.id = awayTeam.id;
                        awayTeamAlreadySaved.teamName = awayTeam.name;
                        await this.teamRepository.manager.save(awayTeamAlreadySaved)
                        this.logger.log(`AWAY TEAM SAVED - TASK ${taskId}`);
                    }

                    const newMatch = new Match();
                    newMatch.id = id;
                    newMatch.date = new Date(utcDate);
                    newMatch.matchStatus = matchStatus;
                    newMatch.awayTeam = awayTeamAlreadySaved;
                    newMatch.homeTeam = homeTeamAlreadySaved;

                    // save in database
                    await this.matchRepository.manager.save(newMatch);
                    this.logger.log(`NEW MATCH ${id} SAVED - TASK ${taskId}`);
                }

            }

            // put cron task status to ended
            task.taskStatus = TaskStatusCron.FINISHED;
            await this.taskService.update(taskId, task);
            this.logger.log(`TASK FINISHED - TASK ${taskId}`);

        } catch (error) {
            const { message } = error;
            if(taskId !== 0) {
                const task = {
                    id: taskId,
                    taskStatus: TaskStatusCron.FINISHED,
                    type: CronType.GET_SCHEDULED,
                    error: message
                };
                // put cron task status to ended and add error
                await this.taskService.update(taskId, task);
            }
            this.logger.error(message);
        }
    }

}
