import { Test, TestingModule } from '@nestjs/testing';
import { ScheduledMatchService } from './scheduled-match.service';

describe('ScheduledMatchService', () => {
  let service: ScheduledMatchService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ScheduledMatchService],
    }).compile();

    service = module.get<ScheduledMatchService>(ScheduledMatchService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
