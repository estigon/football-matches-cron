import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task, Match, MatchStatus, Team } from './entities';
import { ScheduledMatchService } from './services/scheduled-match/scheduled-match.service';
import { TaskService } from './services/task/task.service';
import { FinishedMatchService } from './services/finished-match/finished-match.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task,Match,MatchStatus,Team])
  ],
  providers: [ScheduledMatchService, TaskService, FinishedMatchService]
})
export class CronModule {}
