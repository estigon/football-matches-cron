import axios from 'axios';
const axiosClient = axios.create();

axiosClient.defaults.headers.common['Content-Type'] = 'application/json';
axiosClient.defaults.headers.common['Accept'] = 'application/json';
axiosClient.defaults.headers.common['X-Auth-Token'] = '2612f666373c4bf099a234a43ead62ee';

export function getRequest(URL) {
    return axiosClient.get(`${URL}`).then(response => response);
}
  
export function postRequest(URL, payload) {
    return axiosClient.post(`${URL}`, payload).then(response => response);
}
  
export function patchRequest(URL, payload) {
    return axiosClient.patch(`${URL}`, payload).then(response => response);
}
  
export function deleteRequest(URL) {
    return axiosClient.delete(`${URL}`).then(response => response);
}

export const baseURL = 'https://api.football-data.org';